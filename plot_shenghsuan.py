from ast import literal_eval

import matplotlib.pyplot as plt
from scipy.stats.mstats import gmean
import os
import pandas as pd

# plt.style.use('report_stylesheet_standard.mplstyle')

import setup_sh
fontsize=9
import seaborn as sns
color_list = sns.color_palette("Set2")


plt.style.use('paper.mplstyle')


class Plotting:
    def __init__(self, mapping_basis=('orthogonal', 'original', 'sines', 'iter')):

        self.mapping_basis = mapping_basis
        self.acc_overlap_vs_chimax()
        # self.acc_overlap_vs_nb_images()




    def acc_overlap_vs_chimax(self):
        fig, ax = plt.subplots(figsize=(3.5, 3.5))
        ax_overlap = ax.twinx()
        savefig_path = 'figures/accuracy_and_overlap_vs_chimax.pdf'
        for basis_idx, basis in enumerate(self.mapping_basis):
            df_path = os.path.join('results', basis, 'results.csv')
            if basis == 'original':
                label = 'non-ortho (d)'
            elif basis == 'orthogonal':
                label = 'ortho (d)'
            elif basis == 'iter':
                df_path = r'data\ShengHsuan-MPS-result\results.csv'
                label = 'non-ortho (p)'
                savefig_path = 'figures/accuracy_and_overlap_vs_chimax_with_iter.pdf'
            else:
                label = basis # 'orthonormal'



            try:
                df = pd.read_csv(df_path, sep=',')
            except FileNotFoundError:
                print(df_path, 'not found')
                continue


            # import pdb;pdb.set_trace()
            if basis == 'iter':
                max_sweeps_df = df[df["chi_max"]<60]
            else:
                max_sweeps_df = df[df['nb_sweeps'] == 3]

            new_df = max_sweeps_df['truncation_overlap'].apply(literal_eval)
            overlap_gmean = new_df.apply(gmean)
            ax.plot(max_sweeps_df['chi_max'], max_sweeps_df['accuracy'], '-o', color=color_list[basis_idx], label=label)


            ax_overlap.plot(max_sweeps_df['chi_max'], overlap_gmean, '--o', color=color_list[basis_idx], label=label)
            ax_overlap.set_ylabel(r'Mean of overlaps $| \langle \Sigma_l^\chi | \Sigma_l \rangle |$', fontsize=fontsize)

        ax.set_xlabel('$\chi$', fontsize=fontsize)
        ax.set_ylabel('Accuracy', fontsize=fontsize)
        if 'iter' in self.mapping_basis:
            ax_overlap.legend(fontsize=fontsize, loc='lower right') # bbox_to_anchor=(.53, .20))
        else:
            ax.legend(fontsize=fontsize, bbox_to_anchor=(.3, .58))
        ax.grid(True)

        plt.title('(b)')
        ax.tick_params(axis='both', which='minor', labelsize=fontsize*0.9)
        ax_overlap.tick_params(axis='both', which='minor', labelsize=fontsize*0.9)

        plt.subplots_adjust(left=0.15, right=0.85, bottom=0.13, top=0.94)
        fig.savefig(savefig_path)

    def acc_overlap_vs_nb_images(self):
        fig, ax = plt.subplots(figsize=(3.5, 3.5))
        ax_overlap = ax.twinx()

        for basis_idx, basis in enumerate(self.mapping_basis):

            if basis == 'original':
                label = 'non-ortho (d)'
            elif basis == 'orthogonal':
                label = 'ortho (d)'
            else:
                label = basis # 'orthonormal'

            df_path = os.path.join('results', basis, 'acc_vs_nb_path.csv')
            try:
                df = pd.read_csv(df_path, sep=',')
            except FileNotFoundError:
                print(df_path, 'not found')
                continue
            df = df.sort_values('nb_images_exact')
            new_df = df['truncation_overlap'].apply(literal_eval)
            overlap_gmean = new_df.apply(gmean)
            ax.plot(df['nb_images_exact'], df['accuracy'], '-o', label=label, color=color_list[basis_idx])
            ax.set_xlabel('# of images', fontsize=fontsize)
            ax.set_ylabel('Accuracy', fontsize=fontsize)
            ax.grid(True)
            ax_overlap.plot(df['nb_images_exact'], overlap_gmean, 'o:', label=label, color=color_list[basis_idx])
            ax_overlap.set_ylabel(r'Mean of overlaps $| \langle \Sigma_l^\chi | \Sigma_l \rangle |$', fontsize=fontsize)
            ax.tick_params(axis='both', which='major', labelsize=fontsize)
            ax.tick_params(axis='both', which='minor', labelsize=fontsize * 0.9)
            ax_overlap.tick_params(axis='both', which='major', labelsize=fontsize)
            ax_overlap.tick_params(axis='both', which='minor', labelsize=fontsize * 0.9)
            ax.legend(fontsize=fontsize)
            plt.subplots_adjust(left=0.15, right=0.85, bottom=0.13, top=0.94)
            fig.savefig('figures/accuracy_and_overlap_vs_nb_images.pdf')


if __name__ == '__main__':
    # Plotting(mapping_basis=['orthogonal', 'original', 'iter'])
    Plotting(mapping_basis=['orthogonal', 'original'])
    plt.show()
